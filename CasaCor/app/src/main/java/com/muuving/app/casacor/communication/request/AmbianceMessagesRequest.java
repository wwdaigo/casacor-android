package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.AmbianceMessagesResponse;
import com.muuving.app.casacor.communication.response.AmbiancesProductsResponse;

import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 08/10/15.
 */
public class AmbianceMessagesRequest extends Requests
{
    public AmbianceMessagesRequest(JSONObject postObject)
    {
        super("ambiance_messages", "POST", new AmbianceMessagesResponse());
    }
}

