package com.muuving.app.casacor.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.toolbar.AmbianceDetailsActivity;
import com.muuving.app.casacor.activities.toolbar.ProfessionalActivity;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.views.ItemListAmbianceView;

import java.util.List;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class AmbiancesAdapter extends ArrayAdapter<Ambiances>
{
    public AmbiancesAdapter(Context context, int resource, List<Ambiances> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ItemListAmbianceView itemListAmbianceView;
        final Ambiances ambiances = super.getItem(position);
        final Context context = parent.getContext();

        if (convertView != null) {
            itemListAmbianceView = (ItemListAmbianceView) convertView;
        } else {
            itemListAmbianceView = (ItemListAmbianceView) View.inflate(context, R.layout.item_list_ambiance, null);
        }

        itemListAmbianceView.setAmbiance(ambiances);

        return itemListAmbianceView;
    }
}
