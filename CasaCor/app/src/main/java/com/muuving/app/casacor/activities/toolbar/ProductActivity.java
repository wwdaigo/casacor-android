package com.muuving.app.casacor.activities.toolbar;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.superclass.ToolBarActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.Products;
import com.muuving.app.casacor.dao.ProductsDao;
import com.muuving.app.casacor.dao.Supplier;
import com.muuving.app.casacor.dao.SupplierDao;
import com.muuving.app.casacor.objects.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import org.w3c.dom.Text;

public class ProductActivity extends ToolBarActivity
{

    private ImageView imageViewProduct;
    private TextView textViewProductName;
    private TextView textViewCategory;
    private TextView textViewSupplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_product);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Ação de inserir o lance", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                bidDialog();
            }
        });


        imageViewProduct = (ImageView)findViewById(R.id.imageViewProduct);
        textViewProductName = (TextView)findViewById(R.id.textViewProductName);
        textViewCategory = (TextView)findViewById(R.id.textViewCategory);
        textViewSupplier = (TextView)findViewById(R.id.textViewSupplier);

        ProductsDao dao = CasaCor.getDaoMaster().newSession().getProductsDao();
        Products product = dao.load(getIntent().getLongExtra("productId", 0));

        SupplierDao supplierDao = CasaCor.getDaoMaster().newSession().getSupplierDao();
        Supplier supplier = supplierDao.load(product.getSupplierId().longValue());


        textViewProductName.setText(product.getName());
        textViewCategory.setText(product.getSegment());
        textViewSupplier.setText(supplier.getName());


        try
        {
            ImageLoader.getInstance().displayImage(Utils.formatImageUrl(product.getPhoto(), 450, 200), imageViewProduct);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    private void bidDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Dê o seu lance!");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //m_Text = input.getText().toString();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

}
