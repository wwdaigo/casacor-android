package com.muuving.app.casacor.communication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.communication.interfaces.RestClientInterface;
import com.muuving.app.casacor.communication.request.AmbiancesProductsRequest;
import com.muuving.app.casacor.communication.request.AmbiancesRequest;
import com.muuving.app.casacor.communication.request.EventRequest;
import com.muuving.app.casacor.communication.request.ProductsRequest;
import com.muuving.app.casacor.communication.request.ProfessionalRequest;
import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.superclass.Responses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by daigomatsuoka on 05/10/15.
 */
public class RestClient
{
    private RestClientInterface mRestClientInterface;

    private static final String RESTURL = "http://api.muuving.com.br/mobile/v1/";
    private static final String TOKEN = "Token token=xCCEPFzQZQxEeSGhjRXs";

    public RestClient(RestClientInterface restClientInterface)
    {
        mRestClientInterface = restClientInterface;
    }

    public void getEvents()
    {
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();
        Requests request = new EventRequest();
        asyncTask.execute(request);
    }


    public void getAmbiances(long eventId)
    {
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();
        Requests request = new AmbiancesRequest(eventId);
        asyncTask.execute(request);
    }

    public void getAmbiancesProducts(long ambianceID)
    {
        Log.i("response", "ambianceID "+ambianceID);
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();
        Requests request = new AmbiancesProductsRequest(ambianceID);
        asyncTask.execute(request);
    }

    public void getProfessional(long id)
    {
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();
        Requests request = new ProfessionalRequest(id);
        asyncTask.execute(request);
    }

    public void getProduct(long id)
    {
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();
        Requests request = new ProductsRequest(id);
        asyncTask.execute(request);
    }

    public void postAmbianceMessages(String name, String email, int ambianceId, String message)
    {
        RestClientAsyncTask asyncTask = new RestClientAsyncTask();

    }

    /* Private methods */

    private boolean isConnected()
    {
        ConnectivityManager connMgr = (ConnectivityManager) CasaCor.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    private String downloadUrl(String myurl, String method) throws IOException
    {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestProperty ("Authorization", TOKEN);

            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();


            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is);

            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String readIt(InputStream inputStream)
    {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();

        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return total.toString();
    }



    private class RestClientAsyncTask extends AsyncTask<Requests, Void, Responses>
    {

        @Override
        protected Responses doInBackground(Requests... params)
        {
            Requests request = params[0];
            String url = RESTURL+request.getRestMethod();

            String method = request.getHttpMethod();

            try
            {
                request.getResponse().setResponseString(downloadUrl(url, method));
            } catch (IOException e)
            {
                mRestClientInterface.onRestClientFailed(request, e.getLocalizedMessage());
                e.printStackTrace();
            }

            return request.getResponse();
        }

        @Override
        protected void onPostExecute(Responses r)
        {
            mRestClientInterface.onRestClientSucceeded(r);
            r.save();
        }
    }
}
