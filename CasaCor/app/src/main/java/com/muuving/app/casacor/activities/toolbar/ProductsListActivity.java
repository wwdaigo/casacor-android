package com.muuving.app.casacor.activities.toolbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.superclass.ToolBarActivity;
import com.muuving.app.casacor.adapters.AmbiancesAdapter;
import com.muuving.app.casacor.adapters.ProductsAdapter;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.communication.RestClient;
import com.muuving.app.casacor.communication.interfaces.RestClientInterface;
import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.AmbiancesProductsResponse;
import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.AmbianceProducts;
import com.muuving.app.casacor.dao.AmbianceProductsDao;
import com.muuving.app.casacor.dao.Products;
import com.muuving.app.casacor.dao.ProductsDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductsListActivity extends ToolBarActivity
{
    private ListView listProducts;

    private long[] photosIds;
    int countCalls = 0;
    long ambianceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_products_list);

        ambianceId = getIntent().getLongExtra("ambianceId", 0);

        listProducts = (ListView) findViewById(R.id.listProducts);

        photosIds = getIntent().getLongArrayExtra("ids");

        final List<Long> productsId = new ArrayList<>();

        RestClient restClient = new RestClient(new RestClientInterface()
        {
            @Override
            public void onRestClientSucceeded(Responses responses)
            {
                AmbiancesProductsResponse response = (AmbiancesProductsResponse)responses;

                JSONArray array = response.getJsonArray();
                int count = array.length();

                for (int c=0; c<count; c++)
                {
                    try
                    {
                        JSONObject line = array.getJSONObject(c);
                        JSONObject product = line.getJSONObject("product");

                        long productId = product.getLong("id");

                        if (!productsId.contains(productId))
                        {
                            productsId.add(productId);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }

                countCalls ++;
                if (countCalls >= photosIds.length)
                {
                    loadProducts(productsId);
                }
            }

            @Override
            public void onRestClientFailed(Requests requests, String error) {
                endLoading();
            }
        });

        for (long id : photosIds)
        {
            restClient.getAmbiancesProducts(id);
        }

        startLoading();


    }

    private void loadProducts(final List<Long> ids)
    {
        countCalls = 0;

        if (ids.size() > 0)
        {
            RestClient restClient = new RestClient(new RestClientInterface() {
                @Override
                public void onRestClientSucceeded(Responses responses)
                {
                    countCalls++;
                    if (countCalls>= ids.size())
                    {
                        listProducts(ids);
                        endLoading();
                    }
                }

                @Override
                public void onRestClientFailed(Requests requests, String error)
                {
                    endLoading();
                }
            });

            for (long id : ids)
            {
                restClient.getProduct(id);
            }
        }
        else
        {
            endLoading();
        }
    }

    private void listProducts(List<Long> ids)
    {
        ProductsDao dao = CasaCor.getDaoMaster().newSession().getProductsDao();
        List<Products> productsList = dao.queryBuilder().where(ProductsDao.Properties.Id.in(ids)).build().list();

        Log.i("productsList", ""+productsList.size());

        listProducts.setAdapter(new ProductsAdapter(this, R.layout.item_list_product, productsList));
    }
}
