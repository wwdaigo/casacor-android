package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.EventResponse;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class EventRequest extends Requests
{
    public EventRequest()
    {
        super("events", "GET", new EventResponse());
    }
}
