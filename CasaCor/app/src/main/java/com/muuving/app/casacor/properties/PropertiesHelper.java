package com.muuving.app.casacor.properties;

import android.content.Context;
import android.content.SharedPreferences;

import com.muuving.app.casacor.application.CasaCor;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class PropertiesHelper
{
    private static SharedPreferences getSharedPreferences()
    {
        Context context = CasaCor.getContext();
        SharedPreferences sharedpreferences = context .getSharedPreferences("CasaCorPrefs", Context.MODE_PRIVATE);

        return sharedpreferences;
    }

    public static void setChosenEdition(long id)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong("editionId", id);
        editor.commit();
    }

    public static long getChosenEdition()
    {
        return getSharedPreferences().getLong("editionId", 0);
    }

    private static String getAmbianceKey(int id)
    {
        String key = "ambiance"+id;
        return key;
    }

    public static void setQuizOpened(int id)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(getAmbianceKey(id), true);
        editor.commit();
    }

    public static boolean isQuizOpened(int id)
    {
        return getSharedPreferences().getBoolean(getAmbianceKey(id), false);
    }



    public static void setName(String name)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("name", name);
        editor.commit();
    }

    public static String getName()
    {
        return getSharedPreferences().getString("name", "");
    }


    public static void setEmail(String email)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("email", email);
        editor.commit();
    }

    public static String getEmail()
    {
        return getSharedPreferences().getString("email", "");
    }
}