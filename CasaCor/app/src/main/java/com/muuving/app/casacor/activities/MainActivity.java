package com.muuving.app.casacor.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.muuving.app.casacor.Fragments.AmbiancesFragment;
import com.muuving.app.casacor.NavigationDrawerFragment;
import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.toolbar.webview.QuizWebViewActivity;
import com.muuving.app.casacor.activities.toolbar.webview.SalesWebViewActivity;
import com.muuving.app.casacor.activities.toolbar.webview.TaxiWebViewActivity;
import com.muuving.app.casacor.activities.toolbar.webview.TicketWebViewActivity;
import com.muuving.app.casacor.adapters.EventsDialogAdapter;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.communication.RestClient;
import com.muuving.app.casacor.communication.interfaces.RestClientInterface;
import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.components.ProgressHUD.ProgressHUD;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.dao.EditionsDao;
import com.muuving.app.casacor.properties.PropertiesHelper;

import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, DialogInterface.OnCancelListener {

    private ProgressHUD progressHUD;
    private TextView lblCity;
    private LinearLayout viewTitleCompose;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewTitleCompose = (LinearLayout)findViewById(R.id.viewTitleCompose);
        lblCity = (TextView)findViewById(R.id.lblCity);

        viewTitleCompose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEvent();
            }
        });


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        loadDataFromCurrentEvent();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        switch (position)
        {

            case 1:
                openActivity(SalesWebViewActivity.class, null);
                break;

            case 2:
                openActivity(TicketWebViewActivity.class, null);
                break;

            case 3:
                openActivity(TaxiWebViewActivity.class, null);
                break;

            case 4:
                openActivity(QuizWebViewActivity.class, null);
                break;

            default:
                openDefaultFragment();
                break;
        }
    }

    private void openFragment(Fragment fragment)
    {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private void openActivity(Class activity, Bundle bundle)
    {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }


    private void openDefaultFragment()
    {
        try
        {
            openFragment(AmbiancesFragment.newInstance(PropertiesHelper.getChosenEdition()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                break;
            case 5:
                mTitle = getString(R.string.title_section5);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen())
        {
            //getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
/*
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            Log.i("searchView",""+menu.findItem(R.id.search));
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
*/
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {

    }


    private void selectEvent()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Selecione o evento");

        EditionsDao editionsDao = CasaCor.getDaoMaster().newSession().getEditionsDao();
        final List<Editions> list = editionsDao.loadAll();

        EventsDialogAdapter adapter = new EventsDialogAdapter(this, R.layout.item_list_edition, list);

        builderSingle.setCancelable(false);

        builderSingle.setAdapter(adapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PropertiesHelper.setChosenEdition(list.get(which).getId());
                        loadDataFromCurrentEvent();
                    }
                });
        builderSingle.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setupEdition()
    {
        EditionsDao editionsDao = CasaCor.getDaoMaster().newSession().getEditionsDao();
        Editions editions = editionsDao.load(PropertiesHelper.getChosenEdition());
        lblCity.setText(editions.getCity().toUpperCase());
    }

    private void loadDataFromCurrentEvent()
    {
        if (PropertiesHelper.getChosenEdition() == 0)
        {
            selectEvent();
        }
        else
        {
            progressHUD = ProgressHUD.show(this,"Carregando", true,true,this);
            setupEdition();


            RestClient restClient = new RestClient(new RestClientInterface() {
                @Override
                public void onRestClientSucceeded(Responses responses)
                {
                    progressHUD.dismiss();
                    openDefaultFragment();
                }

                @Override
                public void onRestClientFailed(Requests requests, String error)
                {
                    progressHUD.dismiss();
                }
            });

            restClient.getAmbiances(PropertiesHelper.getChosenEdition());
        }
    }
}
