package com.muuving.app.casacor.communication.response;

import android.util.Log;

import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.AmbiancesDao;
import com.muuving.app.casacor.dao.Products;
import com.muuving.app.casacor.dao.ProductsDao;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;
import com.muuving.app.casacor.dao.Supplier;
import com.muuving.app.casacor.dao.SupplierDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ProductsResponse extends Responses
{
    public ProductsResponse(long id) {
        super(id);
    }

    public void save()
    {
        ProductsDao productsDao = getDaoSession().getProductsDao();
        SupplierDao supplierDao = getDaoSession().getSupplierDao();


        try
        {
            JSONObject object = this.mJsonObject;
            JSONObject supplierObject = object.getJSONObject("supplier");


            Products products = new Products();

            products.setId(object.getLong("id"));
            products.setName(object.getString("name"));
            products.setSegment(object.getString("segment"));
            products.setPhoto(object.getString("photo"));
            products.setSupplierId(supplierObject.getInt("id"));
            productsDao.insertOrReplace(products);



            Supplier supplier = new Supplier();
            supplier.setId(supplierObject.getLong("id"));
            supplier.setName(supplierObject.getString("name"));

            supplierDao.insertOrReplace(supplier);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


    }
}