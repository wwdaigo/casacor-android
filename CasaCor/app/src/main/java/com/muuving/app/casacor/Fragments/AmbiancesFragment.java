package com.muuving.app.casacor.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.adapters.AmbiancesAdapter;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.AmbiancesDao;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AmbiancesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AmbiancesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AmbiancesFragment extends Fragment {

    private ListView listView;

    private static final String ARG_EVENT = "event";

    private long mEvent;

 //   private OnFragmentInteractionListener mListener;

    public static AmbiancesFragment newInstance(long event) {
        AmbiancesFragment fragment = new AmbiancesFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    public AmbiancesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEvent = getArguments().getLong(ARG_EVENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ambiances, container, false);

        listView = (ListView)view.findViewById(R.id.listView);

        AmbiancesDao ambiancesDao = CasaCor.getDaoMaster().newSession().getAmbiancesDao();
        List<Ambiances> list = ambiancesDao.queryBuilder().where(AmbiancesDao.Properties.EditionId.eq(mEvent)).orderAsc(AmbiancesDao.Properties.Position).build().list();

        listView.setAdapter(new AmbiancesAdapter(getActivity(), R.layout.item_list_ambiance, list));

        return view;
    }
/*
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        superclass.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        superclass.onDetach();
        mListener = null;
    }
*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAmbiancesFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
