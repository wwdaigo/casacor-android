package com.muuving.app.casacor.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.objects.MenuData;
import com.muuving.app.casacor.views.ItemListEditionView;
import com.muuving.app.casacor.views.ItemListMenuView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class EventsDialogAdapter extends ArrayAdapter<Editions>
{
    public EventsDialogAdapter(Context context, int resource, List<Editions> objects)
    {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ItemListEditionView itemListEditionView;
        final Editions editions= super.getItem(position);
        final Context context = parent.getContext();

        if (convertView != null) {
            itemListEditionView = (ItemListEditionView) convertView;
        } else {
            itemListEditionView = (ItemListEditionView) View.inflate(context, R.layout.item_list_edition, null);
        }

        itemListEditionView.setEdition(editions);

        return itemListEditionView;
    }
}
