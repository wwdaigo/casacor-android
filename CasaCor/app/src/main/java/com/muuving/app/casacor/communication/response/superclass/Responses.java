package com.muuving.app.casacor.communication.response.superclass;

import android.util.Log;

import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.DaoMaster;
import com.muuving.app.casacor.dao.DaoSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public abstract class Responses
{
    protected JSONObject mJsonObject;
    protected JSONArray mJsonArray;

    private DaoSession session;

    protected long foreingKey;

    public Responses()
    {
        initDao();
    }

    public Responses(long fk)
    {
        foreingKey = fk;
        initDao();
    }

    private void initDao()
    {
        DaoMaster master = CasaCor.getDaoMaster();
        session = master.newSession();
    }

    public void setResponseString(String s)
    {
        try
        {
            if (s.substring(0,1).equals("["))
            {
                mJsonArray = new JSONArray(s);
            }
            else
            {
                mJsonObject = new JSONObject(s);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    protected DaoSession getDaoSession()
    {
        return session;
    }

    public void save()
    {

    }

    public JSONObject getJsonObject()
    {
        return mJsonObject;
    }

    public JSONArray getJsonArray()
    {
        return mJsonArray;
    }
}
