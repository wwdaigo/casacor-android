package com.muuving.app.casacor.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.toolbar.ProductActivity;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.Products;
import com.muuving.app.casacor.views.ItemListAmbianceView;
import com.muuving.app.casacor.views.ItemListProductView;

import java.util.List;

/**
 * Created by daigomatsuoka on 05/11/15.
 */
public class ProductsAdapter extends ArrayAdapter<Products>
{
    public ProductsAdapter(Context context, int resource, List<Products> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ItemListProductView itemListProductView;
        final Products product = super.getItem(position);
        final Context context = parent.getContext();

        if (convertView != null)
        {
            itemListProductView = (ItemListProductView) convertView;
        }
        else {
            itemListProductView = (ItemListProductView) View.inflate(context, R.layout.item_list_product, null);
        }

        itemListProductView.setProduct(product);

        itemListProductView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context,ProductActivity.class);
                intent.putExtra("productId", product.getId());
                context.startActivity(intent);
            }
        });

        return itemListProductView;
    }
}
