package com.muuving.app.casacor.communication.response;

import android.util.Log;

import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.dao.EditionsDao;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class EventResponse extends Responses
{
    public void save()
    {
        int numItems = this.mJsonArray.length();
        Log.e("numItems", ""+numItems);
        EditionsDao editionsDao = getDaoSession().getEditionsDao();

        for (int c = 0; c<numItems; c++)
        {
            try {
                JSONObject object = this.mJsonArray.getJSONObject(c);

                Editions editions = new Editions();
                editions.setId((long) object.getInt("id"));
                editions.setName(object.getString("name"));
                editions.setLocation(object.getString("venue"));
                editions.setCity(object.getString("address_city"));
                editions.setState(object.getString("address_state"));
//                editions.setLatitude((float) object.getDouble("lat"));
//                editions.setLongitude((float) object.getDouble("long"));
                editions.setTicketUrl(object.getString("ticket_url"));
                editions.setVotingUrl(object.getString("voting_url"));
                editions.setMap(object.getString("map"));


                editionsDao.insertOrReplace(editions);

            } catch (JSONException e)
            {
                e.printStackTrace();
            }

        }
    }
}
