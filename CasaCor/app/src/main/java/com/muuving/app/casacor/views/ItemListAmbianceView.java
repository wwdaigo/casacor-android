package com.muuving.app.casacor.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.LinkAddress;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.toolbar.AmbianceDetailsActivity;
import com.muuving.app.casacor.activities.toolbar.ProfessionalActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;
import com.muuving.app.casacor.objects.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.List;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ItemListAmbianceView extends LinearLayout
{
    public ProgressBar loading;
    public ImageView imgAmbiance;
    public TextView txtName;
    public ImageView imgProfessional1;
    public ImageView imgProfessional2;
    public ImageView imgProfessional3;

    public RelativeLayout viewProfessional2;
    public RelativeLayout viewProfessional3;

    public ProgressBar progressProfessional1;
    public ProgressBar progressProfessional2;
    public ProgressBar progressProfessional3;

    public ItemListAmbianceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate()
    {
        imgAmbiance = (ImageView) findViewById(R.id.imgAmbiance);
        txtName = (TextView) findViewById(R.id.txtName);
        imgProfessional1 = (ImageView) findViewById(R.id.imgProfessional1);
        imgProfessional2 = (ImageView) findViewById(R.id.imgProfessional2);
        imgProfessional3 = (ImageView) findViewById(R.id.imgProfessional3);
        loading = (ProgressBar) findViewById(R.id.loading);

        viewProfessional2 = (RelativeLayout) findViewById(R.id.viewProfessional2);
        viewProfessional3 = (RelativeLayout) findViewById(R.id.viewProfessional3);

        progressProfessional1 = (ProgressBar) findViewById(R.id.progressProfessional1);
        progressProfessional2 = (ProgressBar) findViewById(R.id.progressProfessional2);
        progressProfessional3 = (ProgressBar) findViewById(R.id.progressProfessional3);

        viewProfessional2.setVisibility(GONE);
        viewProfessional3.setVisibility(GONE);

        super.onFinishInflate();
    }

    public void setAmbiance(final Ambiances ambiance)
    {
        AmbiancePhotosDao ambiancePhotosDao = CasaCor.getDaoMaster().newSession().getAmbiancePhotosDao();
        List<AmbiancePhotos> list = ambiancePhotosDao.queryBuilder().where(AmbiancePhotosDao.Properties.AmbianceId.eq(ambiance.getId())).build().list();

        AmbianceProfessionalDao ambianceProfessionalDao = CasaCor.getDaoMaster().newSession().getAmbianceProfessionalDao();
        List<AmbianceProfessional> listP = ambianceProfessionalDao.queryBuilder().where(AmbianceProfessionalDao.Properties.AmbianceId.eq(ambiance.getId())).build().list();


        ProfessionalsDao professionalsDao = CasaCor.getDaoMaster().newSession().getProfessionalsDao();
        int countProfessionals = listP.size();

        final Professionals professionals = professionalsDao.load((long) listP.get(0).getProfessionalId());


        try
        {
            ImageLoader.getInstance().displayImage(Utils.formatImageUrl(list.get(0).getPhoto(), 450, 300), imgAmbiance, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    loading.setVisibility(VISIBLE);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    loadPlaceholder();
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    loading.setVisibility(GONE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    loadPlaceholder();
                }
            });
        }
        catch (Exception e)
        {
            loadPlaceholder();
            e.printStackTrace();
        }

        txtName.setText(ambiance.getName().toUpperCase());
        imgProfessional1.setImageBitmap(null);
        ImageLoader.getInstance().displayImage(Utils.formatFaceImageUrl(professionals.getPhoto(), 40, 40), imgProfessional1, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                progressProfessional1.setVisibility(GONE);
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                progressProfessional1.setVisibility(GONE);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {
                progressProfessional1.setVisibility(GONE);
            }
        });


        imgProfessional1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfessionalActivity.class);
                intent.putExtra("id", professionals.getId());
                getContext().startActivity(intent);
            }
        });

        Log.w("countProfessionals", ambiance.getName() + ": " + countProfessionals);
        if (countProfessionals > 1)
        {
            viewProfessional2.setVisibility(VISIBLE);
            final Professionals professionals2 = professionalsDao.load((long) listP.get(1).getProfessionalId());

            Log.w("countProfessionals", ambiance.getName() + ": " + countProfessionals + " - "+professionals2.getName());

            imgProfessional2.setImageBitmap(null);
            ImageLoader.getInstance().displayImage(Utils.formatFaceImageUrl(professionals2.getPhoto(), 40, 40), imgProfessional2, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    progressProfessional2.setVisibility(GONE);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    progressProfessional2.setVisibility(GONE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    progressProfessional2.setVisibility(GONE);
                }
            });

            imgProfessional2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ProfessionalActivity.class);
                    intent.putExtra("id", professionals2.getId());
                    getContext().startActivity(intent);
                }
            });
        }
        else
        {
            viewProfessional2.setVisibility(GONE);
        }
        if (countProfessionals > 2)
        {
            viewProfessional3.setVisibility(VISIBLE);
            final Professionals professionals3 = professionalsDao.load((long) listP.get(2).getProfessionalId());

            imgProfessional3.setImageBitmap(null);
            ImageLoader.getInstance().displayImage(Utils.formatFaceImageUrl(professionals3.getPhoto(), 40, 40), imgProfessional3, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    progressProfessional3.setVisibility(GONE);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    progressProfessional3.setVisibility(GONE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    progressProfessional3.setVisibility(GONE);
                }
            });

            imgProfessional3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ProfessionalActivity.class);
                    intent.putExtra("id", professionals3.getId());
                    getContext().startActivity(intent);
                }
            });
        }
        else
        {
            viewProfessional3.setVisibility(GONE);
        }

        Log.e("VISIBITLY ", ambiance.getName() + " v1" + viewProfessional2.getVisibility());

        imgAmbiance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AmbianceDetailsActivity.class);
                intent.putExtra("id", ambiance.getId());
                getContext().startActivity(intent);
            }
        });
    }

    private void loadPlaceholder()
    {
        imgAmbiance.setImageResource(R.drawable.placeholder);
        loading.setVisibility(GONE);
    }
}
