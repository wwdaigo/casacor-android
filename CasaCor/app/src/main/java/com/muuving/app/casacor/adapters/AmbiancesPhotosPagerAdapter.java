package com.muuving.app.casacor.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.muuving.app.casacor.Fragments.AmbiancesPhotosFragment;
import com.muuving.app.casacor.dao.AmbiancePhotos;

import java.util.List;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class AmbiancesPhotosPagerAdapter extends FragmentStatePagerAdapter
{
    List<AmbiancePhotos> list;

    public AmbiancesPhotosPagerAdapter(FragmentManager fm, List<AmbiancePhotos> listAmb)
    {
        super(fm);
        list = listAmb;
    }

    @Override
    public Fragment getItem(int position) {
        return AmbiancesPhotosFragment.newInstance(list.get(position).getId());
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
