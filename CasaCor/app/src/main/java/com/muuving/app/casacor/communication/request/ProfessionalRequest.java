package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.ProfessionalResponse;
import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ProfessionalRequest extends Requests
{
    public ProfessionalRequest(long id)
    {
        super("professionals/"+id, "GET", new ProfessionalResponse(id));
    }
}
