package com.muuving.app.casacor.communication.response;

import android.util.Log;

import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.AmbiancesDao;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ProfessionalResponse extends Responses
{
    public ProfessionalResponse(long id) {
        super(id);
    }

    public void save()
    {
        ProfessionalsDao professionalsDao = getDaoSession().getProfessionalsDao();

        try
        {
            JSONObject object = this.mJsonObject;

            Professionals professionals = new Professionals();

            professionals.setId(object.getLong("id"));
            professionals.setEmail(object.getString("email"));
            professionals.setWebsite(object.getString("site"));

            professionalsDao.insertOrReplace(professionals);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }
}