package com.muuving.app.casacor.communication.interfaces;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public interface RestClientInterface
{
    void onRestClientSucceeded(Responses responses);
    void onRestClientFailed(Requests requests, String error);
}