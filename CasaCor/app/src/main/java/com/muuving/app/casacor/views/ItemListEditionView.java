package com.muuving.app.casacor.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.dao.Editions;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ItemListEditionView extends LinearLayout
{
    public ItemListEditionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextView txtName;
    public TextView txtVenue;


    @Override
    protected void onFinishInflate()
    {
        txtName = (TextView) findViewById(R.id.txtName);
        txtVenue = (TextView) findViewById(R.id.txtVenue);


        super.onFinishInflate();
    }

    public void setEdition(Editions edition)
    {
        txtName.setText(edition.getName());
        txtVenue.setText(edition.getLocation());
    }
}
