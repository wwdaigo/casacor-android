package com.muuving.app.casacor.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.muuving.app.casacor.dao.Products;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "PRODUCTS".
*/
public class ProductsDao extends AbstractDao<Products, Long> {

    public static final String TABLENAME = "PRODUCTS";

    /**
     * Properties of entity Products.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Name = new Property(1, String.class, "name", false, "NAME");
        public final static Property Price = new Property(2, Float.class, "price", false, "PRICE");
        public final static Property Segment = new Property(3, String.class, "segment", false, "SEGMENT");
        public final static Property SupplierId = new Property(4, Integer.class, "supplierId", false, "SUPPLIER_ID");
        public final static Property Photo = new Property(5, String.class, "photo", false, "PHOTO");
    };


    public ProductsDao(DaoConfig config) {
        super(config);
    }
    
    public ProductsDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"PRODUCTS\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"NAME\" TEXT," + // 1: name
                "\"PRICE\" REAL," + // 2: price
                "\"SEGMENT\" TEXT," + // 3: segment
                "\"SUPPLIER_ID\" INTEGER," + // 4: supplierId
                "\"PHOTO\" TEXT);"); // 5: photo
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"PRODUCTS\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Products entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(2, name);
        }
 
        Float price = entity.getPrice();
        if (price != null) {
            stmt.bindDouble(3, price);
        }
 
        String segment = entity.getSegment();
        if (segment != null) {
            stmt.bindString(4, segment);
        }
 
        Integer supplierId = entity.getSupplierId();
        if (supplierId != null) {
            stmt.bindLong(5, supplierId);
        }
 
        String photo = entity.getPhoto();
        if (photo != null) {
            stmt.bindString(6, photo);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Products readEntity(Cursor cursor, int offset) {
        Products entity = new Products( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // name
            cursor.isNull(offset + 2) ? null : cursor.getFloat(offset + 2), // price
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // segment
            cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4), // supplierId
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5) // photo
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Products entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setPrice(cursor.isNull(offset + 2) ? null : cursor.getFloat(offset + 2));
        entity.setSegment(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setSupplierId(cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4));
        entity.setPhoto(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Products entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Products entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
