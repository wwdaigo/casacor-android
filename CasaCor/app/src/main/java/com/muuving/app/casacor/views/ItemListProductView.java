package com.muuving.app.casacor.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.toolbar.AmbianceDetailsActivity;
import com.muuving.app.casacor.activities.toolbar.ProfessionalActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.Products;
import com.muuving.app.casacor.dao.ProductsDao;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;
import com.muuving.app.casacor.objects.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.List;


/**
 * Created by daigomatsuoka on 05/11/15.
 */
public class ItemListProductView extends LinearLayout
{
    private ImageView imgProduct;
    private TextView lblName;
    private ProgressBar loading;

    public ItemListProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onFinishInflate()
    {
        imgProduct = (ImageView) findViewById(R.id.imgProduct);
        lblName = (TextView) findViewById(R.id.lblName);
        loading = (ProgressBar) findViewById(R.id.loading);

        super.onFinishInflate();
    }

    public void setProduct(final Products product)
    {
        try
        {
            ImageLoader.getInstance().displayImage(Utils.formatImageUrl(product.getPhoto(), 450, 200), imgProduct, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view)
                {
                    loading.setVisibility(VISIBLE);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    loadPlaceholder();
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    loading.setVisibility(GONE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    loadPlaceholder();
                }
            });
        }
        catch (Exception e)
        {
            loadPlaceholder();
            e.printStackTrace();
        }

        lblName.setText(product.getName().toUpperCase());
/*
        imgProduct.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AmbianceDetailsActivity.class);
                intent.putExtra("id", ambiance.getId());
                getContext().startActivity(intent);
            }
        });*/
    }

    private void loadPlaceholder()
    {
        imgProduct.setImageResource(R.drawable.placeholder);
        loading.setVisibility(GONE);
    }
}
