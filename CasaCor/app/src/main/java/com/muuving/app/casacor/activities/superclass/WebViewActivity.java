package com.muuving.app.casacor.activities.superclass;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.components.ProgressHUD.ProgressHUD;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class WebViewActivity extends ToolBarActivity
{
    private WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_web_view);

        this.webView = (WebView)findViewById(R.id.webview);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);

                endLoading();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
    }


    protected void openURL(String url)
    {
        startLoading();
        webView.loadUrl(url);
    }

    protected void loadHTML(String html)
    {

    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }
}
