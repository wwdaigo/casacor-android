package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.ProductsResponse;
import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ProductsRequest extends Requests
{
    public ProductsRequest(long id)
    {
        super("products/"+id, "GET", new ProductsResponse(id));
    }
}
