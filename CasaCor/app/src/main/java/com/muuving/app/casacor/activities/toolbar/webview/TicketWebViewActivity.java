package com.muuving.app.casacor.activities.toolbar.webview;

import android.os.Bundle;
import android.util.Log;

import com.muuving.app.casacor.activities.superclass.WebViewActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.dao.EditionsDao;
import com.muuving.app.casacor.properties.PropertiesHelper;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class TicketWebViewActivity extends WebViewActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        EditionsDao editionsDao = CasaCor.getDaoMaster().newSession().getEditionsDao();
        Editions editions = editionsDao.load(PropertiesHelper.getChosenEdition());
        openURL(editions.getTicketUrl());

    }
}
