package com.muuving.app.casacor.activities.toolbar.webview;

import android.os.Bundle;

import com.muuving.app.casacor.activities.superclass.WebViewActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.dao.EditionsDao;
import com.muuving.app.casacor.properties.PropertiesHelper;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class TaxiWebViewActivity extends WebViewActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        EditionsDao editionsDao = CasaCor.getDaoMaster().newSession().getEditionsDao();
        Editions editions = editionsDao.load(PropertiesHelper.getChosenEdition());

        openURL("https://webapp-ryu.easytaxi.net.br/external/partner/?pick="+editions.getLatitude()+","+editions.getLongitude()+",17z&refp=home&dest=-23.536634,-46.731873,17z&refd=job&part=casacor&utm_campaign=casa_cor_partner&utm_source=casa_cor&utm_medium=link&lang=en_us");

    }
}
