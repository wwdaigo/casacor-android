package com.muuving.app.casacor.objects;

import android.media.Image;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class MenuData
{
    private int mIconResource;
    private String mTitle;

    public MenuData(int icon, String title)
    {
        mIconResource = icon;
        mTitle = title;
    }

    public int getIconResource()
    {
        return mIconResource;
    }

    public String getTitle()
    {
        return mTitle;
    }
}
