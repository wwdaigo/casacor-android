package com.muuving.app.casacor.communication.response;

import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbianceProducts;
import com.muuving.app.casacor.dao.AmbianceProductsDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.Professionals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class AmbiancesProductsResponse extends Responses
{
    public AmbiancesProductsResponse(long ambianceId)
    {
        super(ambianceId);
    }

    public void save()
    {
        int numItems = this.mJsonArray.length();

        AmbianceProductsDao ambianceProductsDao = getDaoSession().getAmbianceProductsDao();

        for (int c = 0; c<numItems; c++)
        {
            try
            {
                JSONObject object = this.mJsonArray.getJSONObject(c);

                AmbianceProducts ambianceProducts = new AmbianceProducts();

                ambianceProducts.setId(object.getLong("id"));
                ambianceProducts.setAmbiancePhotoId((int) this.foreingKey);
                ambianceProducts.setPosX((float) object.getDouble("x"));
                ambianceProducts.setPosY((float) object.getDouble("y"));

                JSONObject objectProduct = object.getJSONObject("product");
                ambianceProducts.setProductId(objectProduct.getInt("id"));

                ambianceProductsDao.insertOrReplace(ambianceProducts);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }
    }
}