package com.muuving.app.casacor.application;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.muuving.app.casacor.dao.DaoMaster;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by daigomatsuoka on 05/10/15.
 */
public class CasaCor extends Application
{
    private static DaoMaster daoMaster;
    private static Context applicationContext;

    public void onCreate() {
        super.onCreate();

        applicationContext = this.getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(applicationContext, "casacor-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
    }

    public static DaoMaster getDaoMaster()
    {
        return daoMaster;
    }

    public static Context getContext() {
        return applicationContext;
    }
}
