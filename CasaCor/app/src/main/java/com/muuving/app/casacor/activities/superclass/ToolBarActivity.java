package com.muuving.app.casacor.activities.superclass;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.components.ProgressHUD.ProgressHUD;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class ToolBarActivity extends AppCompatActivity implements DialogInterface.OnCancelListener {
    private ProgressHUD progressHUD;

    protected void onCreate(Bundle savedInstanceState, int layoutId)
    {
        super.onCreate(savedInstanceState);

        setContentView(layoutId);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void startLoading()
    {
        progressHUD = ProgressHUD.show(this,"Carregando", true,true,this);
    }

    protected void endLoading()
    {
        progressHUD.dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }
}
