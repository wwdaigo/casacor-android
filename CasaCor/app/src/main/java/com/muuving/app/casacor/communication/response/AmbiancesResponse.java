package com.muuving.app.casacor.communication.response;

import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessional;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;
import com.muuving.app.casacor.dao.Ambiances;
import com.muuving.app.casacor.dao.AmbiancesDao;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class AmbiancesResponse extends Responses
{
    public AmbiancesResponse(long eventId)
    {
        super(eventId);
    }

    public void save()
    {
        int numItems = this.mJsonArray.length();

        AmbiancesDao ambiancesDao = getDaoSession().getAmbiancesDao();
        AmbiancePhotosDao ambiancePhotosDao = getDaoSession().getAmbiancePhotosDao();
        AmbianceProfessionalDao ambianceProfessionalDao = getDaoSession().getAmbianceProfessionalDao();
        ProfessionalsDao professionalsDao = getDaoSession().getProfessionalsDao();

        for (int c = 0; c<numItems; c++)
        {
            try
            {
                JSONObject object = this.mJsonArray.getJSONObject(c);

                Ambiances ambiances = new Ambiances();

                ambiances.setEditionId((int) this.foreingKey);
                ambiances.setId(object.getLong("id"));
                ambiances.setName(object.getString("name"));
                ambiances.setPosition((object.get("position") == null)? 0:object.getInt("position"));

            /*    if (object.get("quiz")!= null)
                {
                    ambiances.setQuiz(object.getBoolean("quiz"));
                    ambiances.setQuizHTML(object.getString("quiz_html"));
                }
*/
                // Photos Array
                JSONArray photosArray = object.getJSONArray("photos");
                int numPhotos = photosArray.length();

                for (int d=0; d< numPhotos; d++)
                {
                    JSONObject objectPhotos = photosArray.getJSONObject(d);

                    AmbiancePhotos ambiancePhotos = new AmbiancePhotos();

                    ambiancePhotos.setId(objectPhotos.getLong("id"));
                    ambiancePhotos.setAmbianceId(object.getInt("id"));
                    ambiancePhotos.setPhoto(objectPhotos.getString("photo"));

                    ambiancePhotosDao.insertOrReplace(ambiancePhotos);
                }

                // Professionals Array
                JSONArray professionalsArray = object.getJSONArray("professionals");
                int numProfessionals = professionalsArray.length();

                for (int d=0; d<numProfessionals; d++)
                {
                    JSONObject objectProfessionals = professionalsArray.getJSONObject(d);

                    Professionals professionals = new Professionals();

                    professionals.setId(objectProfessionals.getLong("id"));
                    professionals.setName(objectProfessionals.getString("name"));
                    professionals.setPhoto(objectProfessionals.getString("picture"));

                    professionalsDao.insertOrReplace(professionals);



                    AmbianceProfessional ambianceProfessional = new AmbianceProfessional();

                    ambianceProfessionalDao.deleteInTx(ambianceProfessionalDao.queryBuilder().where(AmbianceProfessionalDao.Properties.AmbianceId.eq(object.getInt("id")))
                            .where(AmbianceProfessionalDao.Properties.ProfessionalId.eq(objectProfessionals.getInt("id"))).build().list());

                    ambianceProfessional.setAmbianceId(object.getInt("id"));
                    ambianceProfessional.setProfessionalId(objectProfessionals.getInt("id"));

                    ambianceProfessionalDao.insertOrReplace(ambianceProfessional);
                }

                ambiancesDao.insertOrReplace(ambiances);

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

    }
}
