package com.muuving.app.casacor.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ImageProfessionalView extends RelativeLayout
{
    private ImageView imgView;

    public ImageProfessionalView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate()
    {
        imgView = (ImageView) findViewById(R.id.imgView);

        super.onFinishInflate();
    }

    public void setImageUrl(String url)
    {
        ImageLoader.getInstance().displayImage(url, imgView);
        Log.i("url",url);
    }

}
