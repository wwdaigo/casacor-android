package com.muuving.app.casacor.Fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.IMediaControllerCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.objects.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

/**
 * Created by daigomatsuoka on 07/10/15.
 */
public class AmbiancesPhotosFragment extends Fragment
{
    private ImageView imageView;
    private long idAmbiancePhoto;
    private ProgressBar progress;

    private AmbiancePhotos ambiancePhotos;

    public static AmbiancesPhotosFragment newInstance(long id) {
        AmbiancesPhotosFragment fragment = new AmbiancesPhotosFragment();
        Bundle args = new Bundle();
        args.putLong("idAmbiancePhoto", id);
        fragment.setArguments(args);
        return fragment;
    }

    public AmbiancesPhotosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idAmbiancePhoto = getArguments().getLong("idAmbiancePhoto");

            AmbiancePhotosDao ambiancePhotosDao = CasaCor.getDaoMaster().newSession().getAmbiancePhotosDao();
            ambiancePhotos = ambiancePhotosDao.load(idAmbiancePhoto);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_ambiances_photos, container, false);

        imageView = (ImageView)rootView.findViewById(R.id.imageView);
        progress = (ProgressBar)rootView.findViewById(R.id.progress);
        ImageLoader.getInstance().displayImage(Utils.formatImageUrl(ambiancePhotos.getPhoto(), 600, 400), imageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason)
            {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {
                progress.setVisibility(View.GONE);
            }
        });

        return rootView;
    }
}
