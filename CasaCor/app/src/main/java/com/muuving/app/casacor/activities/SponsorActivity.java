package com.muuving.app.casacor.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.communication.RestClient;
import com.muuving.app.casacor.communication.interfaces.RestClientInterface;
import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.superclass.Responses;

public class SponsorActivity extends AppCompatActivity
{

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor);


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                RestClient restClient = new RestClient(new RestClientInterface() {
                    @Override
                    public void onRestClientSucceeded(Responses responses) {
                        Log.i("responses",responses.getJsonArray().toString());
                        Intent i = new Intent(SponsorActivity.this, MainActivity.class);
                        startActivity(i);

                        // close this activity
                        finish();
                    }

                    @Override
                    public void onRestClientFailed(Requests requests, String error) {

                    }
                });
                restClient.getEvents();

            }
        }, SPLASH_TIME_OUT);
    }
}
