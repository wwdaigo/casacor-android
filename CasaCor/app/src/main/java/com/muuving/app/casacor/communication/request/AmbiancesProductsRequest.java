package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.AmbiancesProductsResponse;
import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class AmbiancesProductsRequest extends Requests
{
    public AmbiancesProductsRequest(long ambianceId)
    {
        super("ambiance_photos/"+ambianceId+"/products", "GET", new AmbiancesProductsResponse(ambianceId));
    }
}
