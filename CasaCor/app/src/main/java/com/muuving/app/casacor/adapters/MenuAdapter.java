package com.muuving.app.casacor.adapters;

import android.content.ClipData;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.objects.MenuData;
import com.muuving.app.casacor.views.ItemListMenuView;

import java.util.List;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class MenuAdapter extends ArrayAdapter<MenuData>
{
    public MenuAdapter(Context context, int resource, List<MenuData> objects)
    {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ItemListMenuView itemListMenuView;
        final MenuData menuData = super.getItem(position);
        final Context context = parent.getContext();

        if (convertView != null)
        {
            itemListMenuView = (ItemListMenuView) convertView;
        }
        else
        {
            itemListMenuView = (ItemListMenuView) View.inflate(context, R.layout.item_list_menu, null);
        }

        itemListMenuView.setMenuData(menuData);

        return itemListMenuView;
    }
}
