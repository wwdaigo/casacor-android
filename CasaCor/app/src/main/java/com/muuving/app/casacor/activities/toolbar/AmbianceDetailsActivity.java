package com.muuving.app.casacor.activities.toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.app.ToolbarActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.muuving.app.casacor.Fragments.AmbiancesPhotosFragment;
import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.superclass.ToolBarActivity;
import com.muuving.app.casacor.adapters.AmbiancesPhotosPagerAdapter;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.AmbiancePhotos;
import com.muuving.app.casacor.dao.AmbiancePhotosDao;
import com.muuving.app.casacor.dao.AmbianceProfessionalDao;

import java.util.ArrayList;
import java.util.List;

public class AmbianceDetailsActivity extends ToolBarActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private List<AmbiancePhotos> list;
    private long ambianceId;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_ambiance_details);

        ambianceId = getIntent().getLongExtra("id", 0);

        AmbiancePhotosDao ambiancePhotosDao = CasaCor.getDaoMaster().newSession().getAmbiancePhotosDao();
        list = ambiancePhotosDao.queryBuilder().where(AmbiancePhotosDao.Properties.AmbianceId.eq(ambianceId)).build().list();

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new AmbiancesPhotosPagerAdapter(getSupportFragmentManager(), list);
        mPager.setAdapter(mPagerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ambiance_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_auctions)
        {
            long[] ids = new long[list.size()];
            int c = 0;
            for (AmbiancePhotos ambiancePhotos : list)
            {
                ids[c] = ambiancePhotos.getId();
                c++;
            }

            Log.i("listIds", ids.toString());
            Intent intent = new Intent(this, ProductsListActivity.class);
            intent.putExtra("ids", ids);
            intent.putExtra("ambianceId", ambianceId);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
