package com.muuving.app.casacor.activities.toolbar.webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.muuving.app.casacor.activities.superclass.WebViewActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.dao.Editions;
import com.muuving.app.casacor.dao.EditionsDao;
import com.muuving.app.casacor.properties.PropertiesHelper;

public class SalesWebViewActivity extends WebViewActivity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        openURL("http://ofertas.muuving.com.br/");
    }
}
