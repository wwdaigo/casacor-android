package com.muuving.app.casacor.communication.request.superclass;

import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public abstract class Requests
{
    protected String restMethod;
    protected String httpMethod;

    protected Responses responses;

    public Requests(String restMethod, String httpMethod, Responses responses)
    {
        this.restMethod = restMethod;
        this.httpMethod = httpMethod;

        this.responses = responses;
    }

    public String getRestMethod()
    {
        return restMethod;
    }

    public String getHttpMethod()
    {
        return httpMethod;
    }

    public Responses getResponse()
    {
        return responses;
    }
}
