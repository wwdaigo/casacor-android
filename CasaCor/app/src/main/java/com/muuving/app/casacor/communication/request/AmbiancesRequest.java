package com.muuving.app.casacor.communication.request;

import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.AmbiancesResponse;
import com.muuving.app.casacor.communication.response.superclass.Responses;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class AmbiancesRequest extends Requests
{
    public AmbiancesRequest(long eventId)
    {
        super("events/"+eventId+"/ambiances", "GET", new AmbiancesResponse(eventId));
    }
}
