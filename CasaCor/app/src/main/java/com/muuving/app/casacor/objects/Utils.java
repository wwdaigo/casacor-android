package com.muuving.app.casacor.objects;

/**
 * Created by daigomatsuoka on 28/10/15.
 */
public class Utils
{
    public static String formatImageUrl(String url, int w, int h)
    {
        String imageUpload = "/image/upload/";
        String[] parts = url.split(imageUpload);
        String formatSize = "w_"+w+",h_"+h+",c_limit/";

        return parts[0]+imageUpload+formatSize+parts[1];
    }

    public static String formatFaceImageUrl(String url, int w, int h)
    {
        try
        {
            String imageUpload = "/image/upload/";
            String[] parts = url.split(imageUpload);
            String formatSize = "w_"+w+",h_"+h+",c_thumb,g_face/";

            return parts[0]+imageUpload+formatSize+parts[1];
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
