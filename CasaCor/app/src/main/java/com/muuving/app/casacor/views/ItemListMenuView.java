package com.muuving.app.casacor.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.objects.MenuData;

/**
 * Created by daigomatsuoka on 06/10/15.
 */
public class ItemListMenuView extends LinearLayout
{
    public ImageView imgIcon;
    public TextView txtTitle;

    public ItemListMenuView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate()
    {
        imgIcon = (ImageView) findViewById(R.id.imgIcon);
        txtTitle = (TextView) findViewById(R.id.txtTitle);


        super.onFinishInflate();
    }

    public void setMenuData(MenuData menuData)
    {
        imgIcon.setImageDrawable(getResources().getDrawable(menuData.getIconResource()));
        txtTitle.setText(menuData.getTitle());
    }
}
