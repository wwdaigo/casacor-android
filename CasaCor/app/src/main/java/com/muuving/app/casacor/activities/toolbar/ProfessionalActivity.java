package com.muuving.app.casacor.activities.toolbar;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.muuving.app.casacor.R;
import com.muuving.app.casacor.activities.superclass.ToolBarActivity;
import com.muuving.app.casacor.application.CasaCor;
import com.muuving.app.casacor.communication.RestClient;
import com.muuving.app.casacor.communication.interfaces.RestClientInterface;
import com.muuving.app.casacor.communication.request.superclass.Requests;
import com.muuving.app.casacor.communication.response.superclass.Responses;
import com.muuving.app.casacor.dao.Professionals;
import com.muuving.app.casacor.dao.ProfessionalsDao;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;

public class ProfessionalActivity extends ToolBarActivity {

    private ImageView imgProfessional;

    private TextView lblNome;

    private ImageView icoEmail;
    private ImageView icoHome;

    private TextView lblEmail;
    private TextView lblHome;

    private Button btnHitEmail;
    private Button btnHitHome;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_professional);

        imgProfessional = (ImageView)findViewById(R.id.imgProfessional);

        lblNome = (TextView)findViewById(R.id.lblNome);

        icoEmail = (ImageView)findViewById(R.id.icoEmail);
        icoHome = (ImageView)findViewById(R.id.icoHome);

        lblEmail = (TextView)findViewById(R.id.lblEmail);
        lblHome = (TextView)findViewById(R.id.lblHome);

        btnHitEmail = (Button)findViewById(R.id.btnHitEmail);
        btnHitHome = (Button)findViewById(R.id.btnHitHome);

        loadData();
    }


    private void loadData()
    {
        startLoading();

        icoEmail.setVisibility(View.GONE);
        icoHome.setVisibility(View.GONE);

        final ProfessionalsDao professionalsDao = CasaCor.getDaoMaster().newSession().getProfessionalsDao();
        Professionals professionals = professionalsDao.load(getIntent().getLongExtra("id", 0));

        lblNome.setText(professionals.getName().toUpperCase());
        ImageLoader.getInstance().displayImage(professionals.getPhoto(), imgProfessional);

        RestClient restClient = new RestClient(new RestClientInterface() {
            @Override
            public void onRestClientSucceeded(final Responses responses)
            {
                try
                {
                    final String email = responses.getJsonObject().getString("email");
                    final String website = responses.getJsonObject().getString("site");

                    lblEmail.setText(email);
                    lblHome.setText(website);

                    if (!lblEmail.getText().equals("")) icoEmail.setVisibility(View.VISIBLE);
                    if (!lblHome.getText().equals("")) icoHome.setVisibility(View.VISIBLE);

                    btnHitEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                            emailIntent.setType("plain/html");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

                            startActivity(Intent.createChooser(emailIntent, "Enviar e-mail..."));
                        }
                    });

                    btnHitHome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("http://"+website));
                            startActivity(i);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }



                endLoading();
            }

            @Override
            public void onRestClientFailed(Requests requests, String error) {
                endLoading();
            }
        });

        restClient.getProfessional(professionals.getId());
    }

}
