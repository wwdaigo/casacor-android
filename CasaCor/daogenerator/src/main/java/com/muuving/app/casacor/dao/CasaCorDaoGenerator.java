package com.muuving.app.casacor.dao;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class CasaCorDaoGenerator
{
    public static void main(String args[]) throws Exception
    {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        Schema schema = new Schema(1, "com.muuving.app.casacor.dao");

        Entity editions = schema.addEntity("Editions");
        editions.addIdProperty();
        editions.addStringProperty("name");
        editions.addStringProperty("city");
        editions.addStringProperty("location");
        editions.addStringProperty("state");
        editions.addStringProperty("ticketUrl");
        editions.addStringProperty("votingUrl");
        editions.addFloatProperty("latitude");
        editions.addFloatProperty("longitude");
        editions.addStringProperty("map");

        Entity ambiances = schema.addEntity("Ambiances");
        ambiances.addIdProperty();
        ambiances.addStringProperty("name");
        ambiances.addIntProperty("editionId");
        ambiances.addIntProperty("position");
        ambiances.addStringProperty("state");
        ambiances.addBooleanProperty("quiz");
        ambiances.addStringProperty("quizHTML");

        Entity products = schema.addEntity("Products");
        products.addIdProperty();
        products.addStringProperty("name");
        products.addFloatProperty("price");
        products.addStringProperty("segment");
        products.addIntProperty("supplierId");
        products.addStringProperty("photo");


        Entity ambianceProducts = schema.addEntity("AmbianceProducts");
        ambianceProducts.addIdProperty();
        ambianceProducts.addIntProperty("ambiancePhotoId");
        ambianceProducts.addFloatProperty("posX");
        ambianceProducts.addFloatProperty("posY");
        ambianceProducts.addIntProperty("productId");


        Entity ambianceProfessional = schema.addEntity("AmbianceProfessional");
        ambianceProfessional.addIdProperty();
        ambianceProfessional.addIntProperty("ambianceId");
        ambianceProfessional.addIntProperty("professionalId");


        Entity professionals = schema.addEntity("Professionals");
        professionals.addIdProperty();
        professionals.addStringProperty("name");
        professionals.addStringProperty("email");
        professionals.addStringProperty("photo");
        professionals.addStringProperty("website");


        Entity ambiancePhotos = schema.addEntity("AmbiancePhotos");
        ambiancePhotos.addIdProperty();
        ambiancePhotos.addIntProperty("ambianceId");
        ambiancePhotos.addStringProperty("photo");


        Entity supplier = schema.addEntity("Supplier");
        supplier.addIdProperty();
        supplier.addBooleanProperty("isPremium");
        supplier.addStringProperty("logo");
        supplier.addStringProperty("name");

        new DaoGenerator().generateAll(schema, s+"/app/src/main/java");
    }
}